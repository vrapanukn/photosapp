import { Component,ViewChild,ElementRef } from '@angular/core';
import {Photo} from './photo.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'photosapp';
  @ViewChild('filterInput') filterInput : ElementRef;
  photos : Photo[]=[
    new Photo (1,'https://upload.wikimedia.org/wikipedia/commons/7/74/At_Manchester_2018_068.jpg','manchester'),
    new Photo (2,'https://cdn.londonandpartners.com/visit/general-london/areas/river/76709-640x360-houses-of-parliament-and-london-eye-on-thames-from-above-640.jpg','london'),
    new Photo (3,'https://ichef.bbci.co.uk/news/660/cpsprodpb/E5A5/production/_104398785_dddb06af-e91d-443b-b14f-0be2e0cad0be.jpg','london'),
    new Photo (4,'https://upload.wikimedia.org/wikipedia/commons/0/0f/Library-of-Birmingham-oblique-crop.jpg','birmingham'),
    new Photo (5,'https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/Birmingham_-_Ringbull_-_Selfridges_5.JPG/2560px-Birmingham_-_Ringbull_-_Selfridges_5.JPG','birmingham'),
    new Photo (6,'https://www.communitycare.co.uk/wp-content/uploads/sites/7/2016/05/Stuart-Black-robertharding-REXShutterstock-birmingham.jpg','birmingham'),
    new Photo (7,'http://www.yepglobal.com/wp-content/uploads/2017/07/shutterstock_625358573-1.jpg','leeds'),
    new Photo (8,'http://www.leeds.ac.uk/images/Corn_exchange.jpg','leeds'),
    new Photo (9,'https://www.retailgazette.co.uk/wp/wp-content/uploads/MoorStreetDay_E.jpg','birmingham'),
  ];
  
  filteredPhotos : Photo[];
  filteredIndex:number[];
  paginateBy:number;
  pages:number[];
  selectedPage:number;

  ngOnInit()
  {
    //slice to get copy of photos as opposed to reference
    this.filteredPhotos=this.photos.slice();
    //iterate through index based on pagination - hardcoded to 3 for now
    this.filteredIndex=Array.from({length: 3}, (v, k) => k); 
    this.paginateBy=3;
    this.selectedPage=1;
    //calc num of pages based on filtered photos and photos per page
    this.pages=Array.from({length: Math.ceil(this.filteredPhotos.length/this.paginateBy)}, (v, k) => k+1); 
   
  }

  onChange()
  {
    this.filteredPhotos.length=0;
      
    //filter on substring of description
    this.filteredPhotos=this.photos.filter(photo=>
      {
        if(photo.description.indexOf(this.filterInput.nativeElement.value)===-1)
        {return false;}
        return true;
      
      });
      //show 3 images per page for now
      this.filteredIndex=Array.from({length: 3}, (v, k) => k); 
      //recalc number of pages after change to filter
      this.pages=Array.from({length: Math.ceil(this.filteredPhotos.length/this.paginateBy)}, (v, k) => k+1); 
      
  }

  onPrevious()
  {
    if(this.selectedPage-1>=1)
    {
      this.onPage(this.selectedPage-1);
    }
  }
  onNext()
  {      
    if(this.pages.length>this.selectedPage)
    {
      this.onPage(this.selectedPage+1);
    }
  }
  onPage(i)
  {
    this.filteredIndex=Array.from({length: this.paginateBy}, (v, k) => k+(this.paginateBy)*(i-1)); 
    this.selectedPage=i;
  }
}

